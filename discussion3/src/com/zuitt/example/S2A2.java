package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
public class S2A2 {
    public static void main(String[] args){
        int[] primeArr = new int[5];

        primeArr[0] = 2;
        primeArr[1] = 3;
        primeArr[2] = 5;
        primeArr[3] = 7;
        primeArr[4] = 11;

        System.out.println("The first prime number is: " + primeArr[0]);

        ArrayList<String> friendsArr = new ArrayList<String>();

        friendsArr.add("Blessy");
        friendsArr.add("Owen");
        friendsArr.add("Chippy");
        friendsArr.add("Tutti");

        System.out.println("My pets are: " + friendsArr);

        HashMap<String, Integer> inventoryHash = new HashMap<String, Integer>(){
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory consists of: " + inventoryHash);
    }
}
